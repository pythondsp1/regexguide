.. Regular expressions documentation master file, created by
   sphinx-quickstart on Mon Feb 20 22:58:01 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Regular expressions's documentation!
===============================================

Contents:

.. toctree::
   :maxdepth: 2
   
   regex/regex



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

